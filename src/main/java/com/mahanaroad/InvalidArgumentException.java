package com.mahanaroad;


/**
 * This exception indicates that an argument passed to a method contains an invalid value.
 */
public class InvalidArgumentException extends MethodArgumentException {
    
    private static final long serialVersionUID = -5423118040791775179L;
    

    private static String createMessage(String message, 
                                        String argumentName, 
                                        Object argumentValue,
                                        Throwable cause) {
        
        StringBuilder sbuf = new StringBuilder();
        
        if (message != null) {
            sbuf.append(message)
                .append(": ");
        } else {
            sbuf.append("A method argument contains an invalid value: ");
        }
        
        if (argumentName != null) {
            sbuf.append("argumentName=[")
                .append(argumentName)
                .append("]");
        }
            
        if (argumentValue != null) {
            sbuf.append(", argumentValue=[")
                .append(argumentValue)
                .append("]");
        }
        
        if (cause != null) {
            sbuf.append(". Nested exception is: ")
                .append(cause);
        }
        
        return sbuf.toString();
            
    }
    
    
    private final String argumentName;
    private final Object argumentValue;

    
    public InvalidArgumentException(String message, String argumentName, Object argumentValue, Throwable cause) {
    
    	super(createMessage(message, argumentName, argumentValue, cause), cause);
        
        this.argumentName = argumentName;
        this.argumentValue = argumentValue;
    
    }

    
    public InvalidArgumentException(String message, String argumentName, Object argumentValue) {
    
    	this(message, argumentName, argumentValue, null);
    
    }

    
    public String getArgumentName() {
    	return this.argumentName;
    }

    
    public Object getArgumentValue() {
    	return this.argumentValue;
    }

    
}
