package com.mahanaroad;


/**
 * A checked exception that indicates a business rule has been violated.
 */
public class BusinessCheckedException extends Exception {


    private static final long serialVersionUID = 8999341922968905012L;


    public BusinessCheckedException() {
        // do nothing
    }


    public BusinessCheckedException(String message) {
        super(message);
    }


    public BusinessCheckedException(Throwable cause) {
        super(cause);
    }


    public BusinessCheckedException(String message, Throwable cause) {
        super(message, cause);
    }


}
