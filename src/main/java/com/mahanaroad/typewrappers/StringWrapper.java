package com.mahanaroad.typewrappers;

import com.mahanaroad.BlankStringException;


public abstract class StringWrapper {


    private final String value;


    /**
     * @param value Must not be blank.
     * @throws BlankStringException if {@code value} is blank.
     */
    protected StringWrapper(String value) {

        BlankStringException.throwIfBlank(value, "value");
        this.value = value;

    }


    @Override
    public final boolean equals(Object other) {

        if (other == this) {
            return true;
        }

        if (other.getClass() != getClass()) {
            return false;
        }

        StringWrapper that = (StringWrapper) other;

        return this.value.equals(that.value);

    }


    @Override
    public final int hashCode() {
        return this.value.hashCode();
    }


    @Override
    public final String toString() {
        return this.value;
    }


    /**
     * @return Never null or blank.
     */
    public final String getValue() {
        return value;
    }


}
