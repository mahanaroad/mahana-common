package com.mahanaroad.typewrappers;

public class IntWrapper {


    private final int value;


    protected IntWrapper(int value) {
        this.value = value;
    }


    @Override
    public final boolean equals(Object other) {

        if (other == this) {
            return true;
        }

        if (other.getClass() != getClass()) {
            return false;
        }

        IntWrapper that = (IntWrapper) other;

        return this.value == that.value;

    }


    @Override
    public final int hashCode() {
        return this.value;
    }


    @Override
    public String toString() {
        return String.valueOf(this.value);
    }


    public final int getValue() {
        return value;
    }


}
