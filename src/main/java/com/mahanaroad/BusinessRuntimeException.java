package com.mahanaroad;


/**
 * A runtime exception that indicates that a business rule has been violated.
 */
public class BusinessRuntimeException extends RuntimeException {


    private static final long serialVersionUID = 8999341922968905012L;


    public BusinessRuntimeException() {
        // do nothing
    }


    public BusinessRuntimeException(String message) {
        super(message);
    }


    public BusinessRuntimeException(Throwable cause) {
        super(cause);
    }


    public BusinessRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }


}
