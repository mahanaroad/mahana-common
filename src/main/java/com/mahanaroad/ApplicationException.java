package com.mahanaroad;


/**
 * This exception indicates that an application-level programming error has 
 * occurred. It should not be thrown for errors such as invalid user input or
 * IO or Database errors. Basically, this represents a defect in the program.
 * The sort of error that could only occur if usually infallible developers have
 * stuffed up.
 *
 */
public class ApplicationException extends RuntimeException {


    private static final long serialVersionUID = 5368610794466379433L;


    public ApplicationException() {
        super();
    }


    public ApplicationException(final String message) {
        super(message);
    }


    public ApplicationException(final String message, final Throwable cause) {
        super(message, cause);
    }


    public ApplicationException(final Throwable cause) {
        super(cause);
    }


}
