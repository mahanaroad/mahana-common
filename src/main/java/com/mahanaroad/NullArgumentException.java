package com.mahanaroad;


/**
 * This exception indicates that a mandatory method parameter is null. The ever
 * present {@link NullPointerException} could have been used instead of 
 * creating a new class but this exception is a descendent of 
 * {@link ApplicationException}, giving us the ability to distinguish this as a 
 * programming error from within our own code rather than a more generic 
 * {@link RuntimeException}. It also provides {@link #throwIfNull(Object, String)} for convenience.
 * Finally, it allows us to write tests that can check that this exception was
 * thrown for the argument that we expect it to be throw for, see {@link #getArgumentName()}.
 *
 */
public class NullArgumentException extends InvalidArgumentException {
    
    private static final long serialVersionUID = -314739253006962423L;


    /**
     * Checks the given value for null and throws a {@code NullArgumentException} if it is.
     */
    public static void throwIfNull(Object value, String name) {
        if (value == null) {
            throw new NullArgumentException(null, name);
        }
    }


    public NullArgumentException(String message, String argumentName) {
        super(createMessage(message, argumentName), argumentName, null);
    }


    private static String createMessage(String message, String argumentName) {
        
        if (message == null && argumentName == null) {
            return null;
        }
        
        if (message == null) {
            return "The '" + argumentName + "' argument cannot be null";
        }
        
        if (argumentName == null) {
            return message;
        }
        
        return message + ": argumentName=[" + argumentName + "]";
        
    }


}
