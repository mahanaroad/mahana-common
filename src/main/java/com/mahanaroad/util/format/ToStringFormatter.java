package com.mahanaroad.util.format;

import com.mahanaroad.BlankStringException;
import com.mahanaroad.NullArgumentException;

import java.util.Collection;
import java.util.List;

/**
 * This class is used to produce a string representation of an object where each 
 * property of the object appears on a new line and nested {@link Formattable}
 * objects are indented and formatted recursively.
 * 
 * Example output:
 * 
 * <pre>
 * User {
 *     name: "Joe Bloggs"
 *     phone: 123456789
 *     address {
 *         street: "123 Main st"
 *         city: "London"
 *         postcode: "EC1 234"
 *     }
 *     roles[0]: "admin"
 *     roles[1]: "operations"
 * }        
 * </pre>
 * 
 */
public final class ToStringFormatter {
    

    private static final String INDENT = "  ";
    
    private final StringBuilder sb = new StringBuilder();
    private int indentLevel;
    
    
    /**
     * @param rootFormattableName Must not be null or blank.
     * @param rootFormattable Must not be null.
     */
    public ToStringFormatter(String rootFormattableName, Object rootFormattable) {
        
        BlankStringException.throwIfBlank(rootFormattableName, "rootFormattableName");
        NullArgumentException.throwIfNull(rootFormattable, "rootFormattable");
        
        append(rootFormattableName, rootFormattable);
        
    }

    
    public final ToStringFormatter append(String propertyName, List<?> propertyValues) {
        
        if (propertyValues == null) {
            append(propertyName, (Object) null);
        } else if (propertyValues.isEmpty()) {
            append(propertyName, "empty");
        } else {
            
            for (int i = 0; i < propertyValues.size(); i++) {
                String propertyNameIndexed = propertyName + "[" + i + "]";
                append(propertyNameIndexed, propertyValues.get(i));
            }
            
        }
        
        return this;
        
    }

    
    public final ToStringFormatter append(String propertyName, Collection<?> propertyValues) {
        
        if (propertyValues == null) {
            append(propertyName, (Object) null);
        } else if (propertyValues.isEmpty()) {
            append(propertyName, "empty");
        } else {
            
            for (Object object : propertyValues) {
                append(propertyName, object);
            }
            
        }
        
        return this;
        
    }
    
    
    /**
     * @param propertyName Must not be null or blank.
     * @param propertyValue May be null or blank.
     * @return Never null.
     */
    public final ToStringFormatter append(String propertyName, Object propertyValue) {
        
        BlankStringException.throwIfBlank(propertyName, "propertyName");
        
        newLineWithIndent();
        
        if (propertyValue instanceof Formattable) {

            final Formattable formattable = (Formattable) propertyValue;
            
            this.sb.append(propertyName).append(" {");
            this.indentLevel++;
            formattable.format(this);
            this.indentLevel--;
            newLineWithIndent();
            this.sb.append("}");
            
        } else {
            appendProperty(propertyName, propertyValue);
        }
        
        return this;
        
    }
    
    
    private void appendProperty(String propertyName, Object propertyValue) {
        
        boolean requiresQuotes = propertyValue instanceof String;
        
        this.sb.append(propertyName);
        this.sb.append(": ");
        
        if (requiresQuotes) {
            this.sb.append("\"");
        }
        
        this.sb.append(propertyValue);
        
        if (requiresQuotes) {
            this.sb.append("\"");
        }
        
    }
    
    
    private void newLineWithIndent() {
        this.sb.append("\n");
        indent();
    }

    
    private void indent() {
        
        for (int i = 0; i < this.indentLevel; i++) {
            this.sb.append(INDENT);
        }
        
    }
    
    
    @Override
    public String toString() {
        return this.sb.toString();
    }
    
    
}
