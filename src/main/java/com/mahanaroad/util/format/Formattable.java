package com.mahanaroad.util.format;


/**
 * To be implemented by classes that wish to use a {@link ToStringFormatter} 
 * to produce a formatted string representing their state. 
 */
public interface Formattable {
    
    
    /**
     * Writes the properties of the implementing class to the given formatter.
     * @param toStringFormatter Never null.
     */
    public void format(ToStringFormatter toStringFormatter);

    
}
