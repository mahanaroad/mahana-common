package com.mahanaroad.util;

import com.mahanaroad.BlankStringException;
import com.mahanaroad.BusinessRuntimeException;


/**
 * A convenience class for retrieving system properties and 
 * throwing exceptions if they don't exist.
 */
public final class SystemProperties {
    
    
    private SystemProperties() {
        //do nothing
    }
    
    
    /**
     * @param propertyName Must not be null or blank.
     * @return Possibly null or blank.
     * @throws BlankStringException if {@code propertyName} is null or blank.
     */
    public static String getOrNull(String propertyName) {
        
        BlankStringException.throwIfBlank(propertyName, "propertyName");
        return System.getProperty(propertyName);

    }
    
    
    /**
     * @param propertyName Must not be null or blank.
     * @return Never null or blank.
     * @throws BlankStringException if {@code propertyName} is null or blank.
     * @throws BusinessRuntimeException if there is no value stored under 
     * the given propertyName or if it stores a blank value.
     */
    public static String get(String propertyName) {
        
        String value = getOrNull(propertyName);
        
        if (StringUtil.isBlank(value)) {
            throw new BusinessRuntimeException("No system property exists under the system property [" + propertyName + "]");
        }
        
        return value;
        
    }

    
    /**
     * @param propertyName Must not be null or blank.
     * @return Never null or blank.
     * @throws BlankStringException if {@code propertyName} is null or blank.
     * @throws BusinessRuntimeException if there is no value stored under 
     * the given propertyName or if it stores a blank value or if it is not an integer.
     */
    public static int getInt(String propertyName) {
        
        String value = get(propertyName);
        
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new BusinessRuntimeException("Expecting an integer value stored under the system property [" 
                                               + propertyName 
                                               + "] but found [" 
                                               + value 
                                               + "]");
        }
        
    }
    
    
    /**
     * @param propertyName Must not be null or blank.
     * @return Possibly null.
     * @throws BlankStringException if {@code propertyName} is null or blank.
     * @throws BusinessRuntimeException if the value stored under the given property is not an integer.
     */
    public static Integer getIntOrNull(String propertyName) {
        
        String value = getOrNull(propertyName);
        
        if (StringUtil.isBlank(value)) {
            return null;
        }
        
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new BusinessRuntimeException("Expecting an integer value stored under the system property [" 
                                               + propertyName 
                                               + "] but found [" 
                                               + value 
                                               + "]");
        }
        
    }

    
    /**
     * @param propertyName Must not be null or blank.
     * @return Never null or blank.
     * @throws BlankStringException if {@code propertyName} is null or blank.
     * @throws BusinessRuntimeException if there is no value stored under 
     * the given propertyName or if it stores a blank value or a non-double.
     */
    public static double getDouble(String propertyName) {
        
        String value = get(propertyName);
        
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e) {
            throw new BusinessRuntimeException("Expecting a double value stored under the system property [" 
                                               + propertyName 
                                               + "] but found [" 
                                               + value 
                                               + "]");
        }
        
    }
    
    
    /**
     * @param propertyName Must not be null or blank.
     * @return Possibly null.
     * @throws BlankStringException if {@code propertyName} is null or blank.
     * @throws BusinessRuntimeException if the value stored under the given property is not an integer.
     */
    public static Double getDoubleOrNull(String propertyName) {
        
        String value = getOrNull(propertyName);
        
        if (StringUtil.isBlank(value)) {
            return null;
        }
        
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e) {
            throw new BusinessRuntimeException("Expecting a double value stored under the system property [" 
                                               + propertyName 
                                               + "] but found [" 
                                               + value 
                                               + "]");
        }
        
    }

    
    /**
     * @param propertyName Must not be null or blank.
     * @return true if a system property exists under the given property name and it is a string 
     * that is equal (ignoring case) to {@code true}.
     * 
     * @throws BlankStringException if {@code propertyName} is null or blank.
     * @throws BusinessRuntimeException if there is no value stored under 
     * the given propertyName or if it stores a blank value.
     */
    public static boolean getBoolean(String propertyName) {
        
        String value = get(propertyName);
        return Boolean.parseBoolean(value);
        
    }
    
    
    /**
     * @param propertyName Must not be null or blank.
     * @return If a system property exists under the given property name, returns true if the string 
     * is equal, ignoring case, to {@code true}, otherwise returns false. Returns null if no system 
     * property exists with the given name.
     * 
     * @throws BlankStringException if {@code propertyName} is null or blank.
     */
    public static Boolean getBooleanOrNull(String propertyName) {
        
        String value = getOrNull(propertyName);
        
        if (StringUtil.isBlank(value)) {
            return null;
        }
        
        return Boolean.parseBoolean(value);
        
    }
    

}
