package com.mahanaroad.util;


/**
 * Contains convenience utility methods for working with strings.
 */
public final class StringUtil {
	
	
	private StringUtil() {
		//do nothing
	}
	
	
	/**
	 * Returns true if {@code text} is not null and has some non-whitespace characters.
	 * 
	 * @param text May be null.
	 * 
	 * @return true if not null and not only whitespace.
	 */
	public static boolean isNotBlank(String text) {
		
		return text != null && !"".equals(text.trim());
		
	}
	
	
	/**
	 * Returns true if {@code text} is null or consists only of whitespace characters.
	 * 
	 * @param text May be null.
	 * @return true if null or only whitespace.
	 */
	public static boolean isBlank(String text) {
		
		return text == null || "".equals(text.trim());
		
	}
	
	
	/**
	 * If the given string consists only of whitespace characters, null is returned.
	 * 
	 * @param text May be null.
	 * @return null if only whitespace, otherwise the original string is returned.
	 */
	public static String convertBlankToNull(String text) {

        if (isBlank(text)) {
            return null;
        } else {
            return text;
        }

	}
	
	
	/**
	 * Converts the given text to an empty string if it is null. Otherwise returns the original text.
	 * @param text The text to be converted. May be null.
	 * @return Never null.
	 */
	public static String convertNullToEmpty(String text) {
	    
	    if (text == null) {
	        return "";
	    } else {
	        return text;
	    }
	    
	}

    
    /**
     * Converts the given text to an empty string if it is blank. Otherwise returns the original text.
     * @param text The text to be converted. May be null.
     * @return Never null.
     */
    public static String convertBlankToEmpty(String text) {
        
        if (isBlank(text)) {
            return "";
        } else {
            return text;
        }
        
    }
    
	
	/**
	 * If the given text is null or only whitespace, null will be returned. Otherwise the input will 
	 * be trimmed and returned. 
	 * @param text The input string.
	 * @return A trimmed version of the input string or null if the input is only whitespace.
	 */
	public static String trimAndConvertBlankToNull(String text) {
	    
	    if (text == null) {
	        return null;
	    } else {
            return convertBlankToNull(text.trim());
        }
	    
	}

	
    /**
     * Provides a null-safe case-sensitive comparison of two strings as per the 
     * {@link Comparable#compareTo(Object)} method. A null string is treated as 
     * coming after a non-null string.
     *
     * @param text1 The first string.
     * @param text2 The second string.
     * @return As per the {@link String#compareTo(String)} method. 
     */
    public static int nullSafeCompare(String text1, String text2) {
        
        return nullSafeCompare(text1, text2, false);
        
    }

    
    /**
     * Provides a null-safe comparison of two strings as per the 
     * {@link Comparable#compareTo(Object)} method. A null string is treated as 
     * coming after a non-null string.
     *
     * @param comp1 The first object. 
     * @param comp2 The second object.
     * @param ignoreCase Set to true to perform a case-insensitive comparison.
     * @return As per the {@link String#compareTo(String)} or {@link String#compareToIgnoreCase(String)} method.
     */
    public static int nullSafeCompare(String comp1, String comp2, boolean ignoreCase) {
        
        if (comp1 == null && comp2 == null) {
            return 0;
        }
        
        if (comp1 == null) {
            return 1;
        }
        
        if (comp2 == null) {
            return -1;
        }
        
        if (ignoreCase) {
            return comp1.compareToIgnoreCase(comp2);
        } else {
            return comp1.compareTo(comp2);
        }
        
    }
    
    
    /**
     * Returns the string value of the given object if it is not null, otherwise returns null.
     * @param object May be null.
     * @return The string value of the given object or null.
     */
    public static String toStringNullSafe(Object object) {
        
        if (object == null) {
            return null;
        } else {
            return object.toString();
        }
        
    }
    
    
    /**
     * @param text The text to be lower cased. May be null.
     * @return The given text lower cased, or null if input is null.
     */
    public static String toLowerCaseNullSafe(String text) {
        
        if (text == null) {
            return null;
        } else {
            return text.toLowerCase();
        }
        
    }

    
    /**
     * @param text The text to be upper cased. May be null.
     * @return The given text upper cased, or null if input is null.
     */
    public static String toUpperCaseNullSafe(String text) {
        
        if (text == null) {
            return null;
        } else {
            return text.toUpperCase();
        }
        
    }
    
    
    /**
     * Converts {@code CamelCase} to {@code camel_case}. 
     * @param input May be null or blank.
     * @return Possibly null.
     */
    public static String convertCamelCaseToSnakeCase(String input) {
        
        if (isBlank(input)) {
            return input;
        }
        
        final char[] chars = input.toCharArray();
        final StringBuilder sb = new StringBuilder();
        boolean firstChar = true;
        
        for (char ch : chars) {
            
            if (Character.isUpperCase(ch)) {
                
                if (firstChar == false) {
                    sb.append("_");
                }
                
                sb.append(Character.toLowerCase(ch));
                
            } else {
                sb.append(ch);
            }
            
            firstChar = false;
            
        }
        
        return sb.toString();
        
    }

    
    public static String convertFirstCharToLowerCase(String input) {
        
        if (isBlank(input)) {
            return input;
        }
        
        if (Character.isLowerCase(input.charAt(0))) {
            return input;
        }
        
        if (input.length() == 1) {
            return input.toLowerCase();
        }
        
        return Character.toLowerCase(input.charAt(0)) + input.substring(1);
        
    }


    /**
     * Returns true if the given input string is not null and contains no whitespace after being trimmed.
     * @param text May be null.
     * @return true if the input contains no intermediate whitespace.
     */
    public static boolean hasNoWhitespace(String text) {
        return isNotBlank(text) && internalHasNoWhitespaces(text.trim());
    }


    private static boolean internalHasNoWhitespaces(String text) {

        final char[] chars = text.toCharArray();

        for (char ch : chars) {
            if (Character.isWhitespace(ch)) {
                return false;
            }
        }

        return true;

    }


    /**
     * A null-safe version of {@code String.format}.
     * @param message A string that may include placeholders to be replaced with the given args. May be null.
     * @param args Used to replace placeholders in the message string.
     * @return Possibly null or blank.
     */
    public static String formatMessage(String message, Object... args) {

        if (StringUtil.isBlank(message)) {
            return message;
        }
        else {
            return String.format(message, args);
        }

    }


}
