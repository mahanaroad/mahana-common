package com.mahanaroad.util;

import java.util.Arrays;

 
/**
 * A convenience class that provides helper methods
 * for implementing an {@code equals()} or {@code hashCode()} method.
 *
 */
public abstract class ObjectUtil {
   
    private static final int THIRTY_TWO_BITS = 32;

    private ObjectUtil() {
        // do nothing
    }
 

    /**
     * Returns true if the two fields are not both null or are not equal to
     * equal other.
     *
     * @param field1 The first field.
     * @param field2 The second field.
     * @return true if the two fields are not both null or are not equal.
     */
    public static boolean fieldsAreNotEqual(boolean field1, boolean field2) {

    	return field1 != field2;
    
    }
   

    /**
     * Returns true if the two fields are not both null or are not equal to
     * equal other.
     *
     * @param field1 The first field.
     * @param field2 The second field.
     * @return true if the two fields are not both null or are not equal.
     */
    public static boolean fieldsAreNotEqual(byte field1, byte field2) {

        return field1 != field2;

    }


    /**
     * Returns true if the two fields are not both null or are not equal to
     * equal other.
     *
     * @param field1 The first field.
     * @param field2 The second field.
     * @return true if the two fields are not both null or are not equal.
     */
    public static boolean fieldsAreNotEqual(short field1, short field2) {

        return field1 != field2;

    }


    /**
     * Returns true if the two fields are not both null or are not equal to
     * equal other.
     *
     * @param field1 The first field.
     * @param field2 The second field.
     * @return true if the two fields are not both null or are not equal.
     */
    public boolean fieldsAreNotEqual(char field1, char field2) {

        return field1 != field2;

    }
   

    /**
     * Returns true if the two fields are not both null or are not equal to
     * equal other.
     *
     * @param field1 The first field.
     * @param field2 The second field.
     * @return true if the two fields are not both null or are not equal.
     */
    public static boolean fieldsAreNotEqual(int field1, int field2) {

    	return field1 != field2;

    }


    /**
     * Returns true if the two fields are not both null or are not equal to
     * equal other.
     *
     * @param field1 The first field.
     * @param field2 The second field.
     * @return true if the two fields are not both null or are not equal.
     */
    public static boolean fieldsAreNotEqual(long field1, long field2) {

        return field1 != field2;

    }


    /**
     * Returns true if the two fields are not both null or are not equal to
     * equal other.
     *
     * @param field1 The first field.
     * @param field2 The second field.
     * @return true if the two fields are not both null or are not equal.
     */
    public static boolean fieldsAreNotEqual(float field1, float field2) {

        return field1 != field2;

    }


    /**
     * Returns true if the two fields are not both null or are not equal to
     * equal other.
     *
     * @param field1 The first field.
     * @param field2 The second field.
     * @return true if the two fields are not both null or are not equal.
     */
    public static boolean fieldsAreNotEqual(double field1, double field2) {

        return field1 != field2;

    }


    /**
     * Returns true if the two fields are not both null or are not equal to
     * equal other.
     *
     * @param field1 The first field.
     * @param field2 The second field.
     * @return true if the two fields are not both null or are not equal.
     */
    public static boolean fieldsAreNotEqual(boolean[] field1, boolean[] field2) {

        return Arrays.equals(field1, field2) == false;

    }
   

    /**
     * Returns true if the two fields are not both null or are not equal to
     * equal other.
     *
     * @param field1 The first field.
     * @param field2 The second field.
     * @return true if the two fields are not both null or are not equal.
     */
    public static boolean fieldsAreNotEqual(byte[] field1, byte[] field2) {

        return Arrays.equals(field1, field2) == false;

    }


    /**
     * Returns true if the two fields are not both null or are not equal to
     * equal other.
     *
     * @param field1 The first field.
     * @param field2 The second field.
     * @return true if the two fields are not both null or are not equal.
     */
    public static boolean fieldsAreNotEqual(short[] field1, short[] field2) {

        return Arrays.equals(field1, field2) == false;

    }


    /**
     * Returns true if the two fields are not both null or are not equal to
     * equal other.
     *
     * @param field1 The first field.
     * @param field2 The second field.
     * @return true if the two fields are not both null or are not equal.
     */
    public static boolean fieldsAreNotEqual(char[] field1, char[] field2) {

        return Arrays.equals(field1, field2) == false;

    }


    /**
     * Returns true if the two fields are not both null or are not equal to
     * equal other.
     *
     * @param field1 The first field.
     * @param field2 The second field.
     * @return true if the two fields are not both null or are not equal.
     */
    public static boolean fieldsAreNotEqual(int[] field1, int[] field2) {

        return Arrays.equals(field1, field2) == false;

    }


    /**
     * Returns true if the two fields are not both null or are not equal to
     * equal other.
     *
     * @param field1 The first field.
     * @param field2 The second field.
     * @return true if the two fields are not both null or are not equal.
     */
    public static boolean fieldsAreNotEqual(long[] field1, long[] field2) {

        return Arrays.equals(field1, field2) == false;

    }


    /**
     * Returns true if the two fields are not both null or are not equal to
     * equal other.
     *
     * @param field1 The first field.
     * @param field2 The second field.
     * @return true if the two fields are not both null or are not equal.
     */
    public static boolean fieldsAreNotEqual(float[] field1, float[] field2) {

        return Arrays.equals(field1, field2) == false;

    }


    /**
     * Returns true if the two fields are not both null or are not equal to
     * equal other.
     *
     * @param field1 The first field.
     * @param field2 The second field.
     * @return true if the two fields are not both null or are not equal.
     */
    public static boolean fieldsAreNotEqual(double[] field1, double[] field2) {

        return Arrays.equals(field1, field2) == false;

    }


    public static boolean fieldsAreNotEqual(double d1, double d2, int numDecimalPlaces) {

        // Use precision like 0.00001 to check if is in range
        double precision = Math.pow(10, -numDecimalPlaces);
        boolean result = Math.abs(d1-d2) > precision;
        return result;

    }


    /**
     * Returns true if the fields are not equal. If both fields are null or {@link Object#equals(Object)} returns true
     * this method will return false.
     *
     * @param field1 The first field.
     * @param field2 The second field.
     * @return true if the two fields are not both null or are not equal.
     */
    public static boolean fieldsAreNotEqual(Object field1, Object field2) {

        if (field1 == null) {
            return field2 != null;
        } else {
            return !field1.equals(field2);
        }

    }


    /**
     * Returns a hashcode value for the given field.
     *
     * @param field The field to be hashed.
     * @return The hashcode for the given field.
     */
    public static int fieldHashCode(boolean field) {

        return field ? 0 : 1;

    }


    /**
     * Returns a hashcode value for the given field.
     *
     * @param field The field to be hashed.
     * @return The hashcode for the given field.
     */
    public static int fieldHashCode(byte field) {

        return (int) field;

    }


    /**
     * Returns a hashcode value for the given field.
     *
     * @param field The field to be hashed.
     * @return The hashcode for the given field.
     */
    public static int fieldHashCode(short field) {

        return (int) field;

    }


    /**
     * Returns a hashcode value for the given field.
     *
     * @param field The field to be hashed.
     * @return The hashcode for the given field.
     */
    public static int fieldHashCode(char field) {

        return (int) field;

    }


    /**
     * Returns a hashcode value for the given field.
     *
     * @param field The field to be hashed.
     * @return The hashcode for the given field.
     */
    public static int fieldHashCode(int field) {

        return field;

    }


    /**
     * Returns a hashcode value for the given field.
     *
     * @param field The field to be hashed.
     * @return The hashcode for the given field.
     */
    public static int fieldHashCode(long field) {

        return (int)  (field ^ (field >>> THIRTY_TWO_BITS));

    }


    /**
     * Returns a hashcode value for the given field.
     *
     * @param field The field to be hashed.
     * @return The hashcode for the given field.
     */
    public static int fieldHashCode(float field) {

        return Float.floatToIntBits(field);

    }


    /**
     * Returns a hashcode value for the given field.
     *
     * @param field The field to be hashed.
     * @return The hashcode for the given field.
     */
    public static int fieldHashCode(double field) {

        return fieldHashCode(Double.doubleToLongBits(field));

    }


    /**
     * Returns a hashcode value for the given field.
     *
     * @param field The field to be hashed.
     * @return The hashcode for the given field.
     */
    public static int fieldHashCode(Object field) {

        return field == null ? 0 : field.hashCode();

    }


    /**
     * Returns a hashcode value for the given field.
     *
     * @param fields The field to be hashed.
     * @return The hashcode for the given field.
     */
    @SuppressWarnings("boxing")
    public static int fieldHashCode(boolean[] fields) {

        if (fields == null || fields.length == 0) {
            return 0;
        }

        int result = 17;

        for (boolean field : fields) {
            result += 37 * fieldHashCode(field);
        }

        return result;

    }
   

    /**
     * Returns a hashcode value for the given field.
     *
     * @param fields The field to be hashed.
     * @return The hashcode for the given field.
     */
    public static int fieldHashCode(byte[] fields) {

        if (fields == null || fields.length == 0) {
            return 0;
        }

        int result = 17;

        for (byte field : fields) {
            result += 37 * fieldHashCode(field);
        }

        return result;

    }


    /**
     * Returns a hashcode value for the given field.
     *
     * @param fields The field to be hashed.
     * @return The hashcode for the given field.
     */
    public static int fieldHashCode(short[] fields) {

        if (fields == null || fields.length == 0) {
            return 0;
        }

        int result = 17;

        for (short field : fields) {
            result += 37 * fieldHashCode(field);
        }

        return result;

    }
   

    /**
     * Returns a hashcode value for the given field.
     *
     * @param fields The field to be hashed.
     * @return The hashcode for the given field.
     */
    public static int fieldHashCode(char[] fields) {

        if (fields == null || fields.length == 0) {
            return 0;
        }

        int result = 17;

        for (char field : fields) {
            result += 37 * fieldHashCode(field);
        }

        return result;

    }

    /**
     * Returns a hashcode value for the given field.
     *
     * @param fields The field to be hashed.
     * @return The hashcode for the given field.
     */
    public static int fieldHashCode(int[] fields) {

        if (fields == null || fields.length == 0) {
            return 0;
        }

        int result = 17;

        for (int field : fields) {
            result += 37 * fieldHashCode(field);
        }

        return result;

    }
   

    /**
     * Returns a hashcode value for the given field.
     *
     * @param fields The field to be hashed.
     * @return The hashcode for the given field.
     */
    public static int fieldHashCode(long[] fields) {

        if (fields == null || fields.length == 0) {
            return 0;
        }

        int result = 17;

        for (long field : fields) {
            result += 37 * fieldHashCode(field);
        }

        return result;

    }
   

    /**
     * Returns a hashcode value for the given field.
     *
     * @param fields The field to be hashed.
     * @return The hashcode for the given field.
     */
    public static int fieldHashCode(float[] fields) {

        if (fields == null || fields.length == 0) {
            return 0;
        }

        int result = 17;

        for (float field : fields) {
            result += 37 * fieldHashCode(field);
        }

        return result;

    }
   

    /**
     * Returns a hashcode value for the given field.
     *
     * @param fields The field to be hashed.
     * @return The hashcode for the given field.
     */
    public static int fieldHashCode(double[] fields) {

        if (fields == null || fields.length == 0) {
            return 0;
        }

        int result = 17;

        for (double field : fields) {
            result += 37 * fieldHashCode(field);
        }

        return result;

    }
   

    /**
     * Returns a hashcode value for the given field.
     *
     * @param fields The field to be hashed.
     * @return The hashcode for the given field.
     */
    public static int fieldHashCode(Object[] fields) {

        if (fields == null || fields.length == 0) {
            return 0;
        }

        int result = 17;

        for (Object field : fields) {
            result += 37 * field.hashCode();
        }

        return result;

    }
   

    /**
     * Provides a null-safe comparison of two comparable objects as per the
     * {@link Comparable#compareTo(Object)} method. A null is treated as
     * coming after a non-null object.
     *
     * @param <T> The type of object being compared.
     * @param comp1 The first object.
     * @param comp2 The second object.
     * @return As per the {@link Comparable#compareTo(Object)} method.
     */
    public static <T> int nullSafeCompare(Comparable<T> comp1, T comp2) {

        if (comp1 == null && comp2 == null) {
            return 0;
        }

        if (comp1 == null) {
            return 1;
        }

        if (comp2 == null) {
            return -1;
        }

        return comp1.compareTo(comp2);

    }
   

    /**
     * @param o1 The first object.
     * @param o2 The second object.
     * @return true if both objects are null or they are equal as per the {@link Object#equals(Object)} method.
     */
    public static boolean nullSafeEquals(Object o1, Object o2) {

        if (o1 == null && o2 == null) {
            return true;
        }

        if (o1 == null) {
            return false;
        }

        return o1.equals(o2);

    }
   

    /**
     * Returns a string representing the short name of the given class.
     * @param clazz
     * @return Never null or blank.
     */
    public static String getUnqualifiedClassName(Class<?> clazz) {

        String name = clazz.getName();

        if (name.lastIndexOf('.') > 0) {
            name = name.substring(name.lastIndexOf('.') + 1); // Map$Entry
        }

        // The $ can be converted to a .
        name = name.replace('$', '.'); // Map.Entry
        return name;

    }
   

}
