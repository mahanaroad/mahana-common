package com.mahanaroad;


/**
 * Indicates that a string was either null, empty or consisted only of whitespace.
 */
public class BlankStringException extends ApplicationException {


    private static final long serialVersionUID = -5423118040791775179L;


    public static void throwIfBlank(String argumentValue, String argumentName) {

    	if (argumentValue == null || "".equals(argumentValue.trim())) {
    		throw new BlankStringException(argumentName);
    	}

    }


    private final String argumentName;


    private BlankStringException(String argumentName) {

    	super("The [" + argumentName + "] argument cannot be null or blank.");
        this.argumentName = argumentName;

    }

    
    public String getArgumentName() {
    	return this.argumentName;
    }


}
