package com.mahanaroad;


/**
 * This exception indicates that an invalid argument has been passed to a method
 * call. The standard Java API class {@link IllegalArgumentException} could have
 * been used instead of creating a new class but this exception is 
 * a descendent of {@link ApplicationException}, giving us the ability to 
 * distinguish this as a programming error from within our own code rather than 
 * a more generic {@link RuntimeException}. In general, rather than throwing 
 * an instance of this class, look at the available subclasses to see if 
 * a more appropriate exception could be thrown.
 *
 */
public class MethodArgumentException extends ApplicationException {

    private static final long serialVersionUID = -2722398672315076480L;
    
    
    public MethodArgumentException() {
        super();
    }

    
    public MethodArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    
    public MethodArgumentException(String message) {
        super(message);
    }

    
    public MethodArgumentException(Throwable cause) {
        super(cause);
    }

}
