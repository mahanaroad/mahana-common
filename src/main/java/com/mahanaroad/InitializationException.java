package com.mahanaroad;


/**
 * This exception indicates that an initialization error has occurred due to 
 * incorrect application configuration. 
 *
 */
public class InitializationException extends ApplicationException {


    private static final long serialVersionUID = 5725503357647646732L;


    public InitializationException() {
        super();
    }


    public InitializationException(final String message) {
        super(message);
    }


    public InitializationException(final String message, final Throwable cause) {
        super(message, cause);
    }


    public InitializationException(final Throwable cause) {
        super(cause);
    }


}
