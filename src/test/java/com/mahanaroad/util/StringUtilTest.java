package com.mahanaroad.util;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class StringUtilTest {


    @Test(dataProvider = "hasTextProvider")
    public void testHasText(String input, boolean expectedResult) {

        final boolean actualResult = StringUtil.isNotBlank(input);
        Assert.assertEquals(actualResult, expectedResult);

    }


    @DataProvider(name = "hasTextProvider")
    public Object[][] provideHasTextSamples() {

        return new Object[][]{
                {" ", false},
                {"\r", false},
                {"\t ", false},
                {"", false},
                {" yep ", true}
        };
    }


    @Test(dataProvider = "hasNoWhitespaceProvider")
    public void testHasNoWhitespace(String input, boolean expectedResult) {

        final boolean actualResult = StringUtil.hasNoWhitespace(input);
        Assert.assertEquals(actualResult, expectedResult);

    }


    @DataProvider(name = "hasNoWhitespaceProvider")
    public Object[][] provideHasNoWhitespaceSamples() {
        return new Object[][]{
                {"bogus", true},
                {" bogus ", true},
                {"  ", false},
                {" some whitespace ", false},
                {null, false}
        };
    }


}