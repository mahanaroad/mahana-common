package com.mahanaroad.util;

import java.util.Arrays;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
 

public class ObjectUtilTest {

    @Test(dataProvider = "booleanArrayDataProvider")
    public void testFieldsAreNotEqualBooleanArray(boolean[] array1, boolean[] array2, boolean expectedResult) {

        boolean actualResult = ObjectUtil.fieldsAreNotEqual(array1, array2);
        Assert.assertEquals(actualResult, expectedResult, "array1=" + Arrays.toString(array1) + ", array2=" + Arrays.toString(array2));

    }


    @DataProvider(name = "booleanArrayDataProvider")
    public Object[][] provideBooleanArrays() {

        return new Object[][] {
                {null, null, false},
                {new boolean[] {}, new boolean[] {}, false},
                {new boolean[] {true}, new boolean[] {true}, false},
                {new boolean[] {true, false}, new boolean[] {true, false}, false},
                {new boolean[] {true}, new boolean[] {false}, true},
                {new boolean[] {true}, new boolean[] {}, true},
        };

    }
   

    @Test(dataProvider = "byteArrayDataProvider")
    public void testFieldsAreNotEqualByteArray(byte[] array1, byte[] array2, boolean expectedResult) {

        boolean actualResult = ObjectUtil.fieldsAreNotEqual(array1, array2);
        Assert.assertEquals(actualResult, expectedResult, "array1=" + Arrays.toString(array1) + ", array2=" + Arrays.toString(array2));

    }
   

    @DataProvider(name = "byteArrayDataProvider")
    public Object[][] provideByteArrays() {

        return new Object[][] {
                {null, null, false},
                {new byte[] {}, new byte[] {}, false},
                {new byte[] {1}, new byte[] {1}, false},
                {new byte[] {1, 100}, new byte[] {1, 100}, false},
                {new byte[] {1}, new byte[] {8}, true},
                {new byte[] {1}, new byte[] {}, true},
        };

    }
   

    @Test(dataProvider = "shortArrayDataProvider")
    public void testFieldsAreNotEqualShortArray(short[] array1, short[] array2, boolean expectedResult) {

        boolean actualResult = ObjectUtil.fieldsAreNotEqual(array1, array2);
        Assert.assertEquals(actualResult, expectedResult, "array1=" + Arrays.toString(array1) + ", array2=" + Arrays.toString(array2));

    }
   

    @DataProvider(name = "shortArrayDataProvider")
    public Object[][] provideShortArrays() {

        return new Object[][] {
                {null, null, false},
                {new short[] {}, new short[] {}, false},
                {new short[] {1}, new short[] {1}, false},
                {new short[] {1, 100}, new short[] {1, 100}, false},
                {new short[] {1}, new short[] {8}, true},
                {new short[] {1}, new short[] {}, true},
        };

    }
   

    @Test(dataProvider = "charArrayDataProvider")
    public void testFieldsAreNotEqualCharArray(char[] array1, char[] array2, boolean expectedResult) {

        boolean actualResult = ObjectUtil.fieldsAreNotEqual(array1, array2);
        Assert.assertEquals(actualResult, expectedResult, "array1=" + Arrays.toString(array1) + ", array2=" + Arrays.toString(array2));

    }
   

    @DataProvider(name = "charArrayDataProvider")
    public Object[][] provideCharArrays() {

        return new Object[][] {
                {null, null, false},
                {new char[] {}, new char[] {}, false},
                {new char[] {'a'}, new char[] {'a'}, false},
                {new char[] {'a', 'b'}, new char[] {'a', 'b'}, false},
                {new char[] {'a'}, new char[] {'b'}, true},
                {new char[] {'a'}, new char[] {}, true},
        };

    }
   

    @Test(dataProvider = "intArrayDataProvider")
    public void testFieldsAreNotEqualIntArray(int[] array1, int[] array2, boolean expectedResult) {
       
        boolean actualResult = ObjectUtil.fieldsAreNotEqual(array1, array2);
        Assert.assertEquals(actualResult, expectedResult, "array1=" + Arrays.toString(array1) + ", array2=" + Arrays.toString(array2));

    }
   

    @DataProvider(name = "intArrayDataProvider")
    public Object[][] provideIntArrays() {

        return new Object[][] {
                {null, null, false},
                {new int[] {}, new int[] {}, false},
                {new int[] {1}, new int[] {1}, false},
                {new int[] {1, 100}, new int[] {1, 100}, false},
                {new int[] {1}, new int[] {8}, true},
                {new int[] {1}, new int[] {}, true},
        };

    }


    @Test(dataProvider = "longArrayDataProvider")
    public void testFieldsAreNotEqualLongArray(long[] array1, long[] array2, boolean expectedResult) {

        boolean actualResult = ObjectUtil.fieldsAreNotEqual(array1, array2);
        Assert.assertEquals(actualResult, expectedResult, "array1=" + Arrays.toString(array1) + ", array2=" + Arrays.toString(array2));

    }
   

    @DataProvider(name = "longArrayDataProvider")
    public Object[][] provideLongArrays() {

        return new Object[][] {
                {null, null, false},
                {new long[] {}, new long[] {}, false},
                {new long[] {1L}, new long[] {1L}, false},
                {new long[] {1L, 100L}, new long[] {1L, 100L}, false},
                {new long[] {1L}, new long[] {8L}, true},
                {new long[] {1L}, new long[] {}, true},
        };
       
    }
   

    @Test(dataProvider = "floatArrayDataProvider")
    public void testFieldsAreNotEqualFloatArray(float[] array1, float[] array2, boolean expectedResult) {

        boolean actualResult = ObjectUtil.fieldsAreNotEqual(array1, array2);
        Assert.assertEquals(actualResult, expectedResult, "array1=" + Arrays.toString(array1) + ", array2=" + Arrays.toString(array2));

    }
   

    @DataProvider(name = "floatArrayDataProvider")
    public Object[][] provideFloatArrays() {

        return new Object[][] {
                {null, null, false},
                {new float[] {}, new float[] {}, false},
                {new float[] {0.0f}, new float[] {0.0f}, false},
                {new float[] {0.0f, 1.23f}, new float[] {0.0f, 1.23f}, false},
                {new float[] {0.0f}, new float[] {0.1f}, true},
                {new float[] {0.0f}, new float[] {}, true},
        };

    }
   

    @Test(dataProvider = "doubleArrayDataProvider")
    public void testFieldsAreNotEqualDoubleArray(double[] array1, double[] array2, boolean expectedResult) {

        boolean actualResult = ObjectUtil.fieldsAreNotEqual(array1, array2);
        Assert.assertEquals(actualResult, expectedResult, "array1=" + Arrays.toString(array1) + ", array2=" + Arrays.toString(array2));

    }
   

    @DataProvider(name = "doubleArrayDataProvider")
    public Object[][] provideDoubleArrays() {

        return new Object[][] {
                {null, null, false},
                {new double[] {}, new double[] {}, false},
                {new double[] {0.0d}, new double[] {0.0d}, false},
                {new double[] {0.0d, 1.23d}, new double[] {0.0d, 1.23d}, false},
                {new double[] {0.0d}, new double[] {0.1d}, true},
                {new double[] {0.0d}, new double[] {}, true},
        };

    }

    
}